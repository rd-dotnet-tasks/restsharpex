﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RestSharpEx.Models;

namespace RestSharpEx.Controllers
{
    public class WeatherController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult QueryWeatherApi(string city)
        {
            string apiEndPoint = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);

            var client = new RestClient(apiEndPoint);
            var request = new RestRequest("/api/WeatherApi", Method.GET);

            request.AddParameter("city", city);

            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            var response = client.Execute<WeatherDataModel>(request);

            WeatherDataModel model = response.Data;

            return View(model);
        }
    }
}