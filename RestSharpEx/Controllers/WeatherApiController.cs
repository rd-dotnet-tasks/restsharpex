﻿using RestSharp;
using System.Web.Http;
using RestSharpEx.Models;

namespace RestSharpEx.Controllers
{
    public class WeatherApiController : ApiController
    {
        private const string _apiEndpoint = "http://api.openweathermap.org/data/2.5/";


        // GET api/weatherapi/hyderabad
        public WeatherDataModel Get(string city)
        {
            var client = new RestClient(_apiEndpoint);
            var request = new RestRequest("/weather/", Method.GET);

            request.AddParameter("q", city);
            request.AddParameter("APPID", "179af0bb7a914e3c893b38e9eff033b9");
            request.AddParameter("units", "metric");

            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            var response = client.Execute<WeatherDataModel>(request);
            return response.Data;
        }
    }
}
